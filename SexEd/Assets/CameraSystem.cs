﻿using UnityEngine;
using System.Collections;

public class CameraSystem : MonoBehaviour {

    Vector3 pos;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        pos = Vector3.Lerp(pos, GameObject.Find("player").transform.position, Time.deltaTime);
        transform.position = pos;
	}
}
