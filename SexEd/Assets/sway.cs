﻿using UnityEngine;
using System.Collections;

public class sway : MonoBehaviour {

    float pos;
    float speed;
    float amount;
	// Use this for initialization
	void Start () {
        //Randomly assign sway Speed and Amount
        speed = Random.RandomRange(1.0f, 1.5f);
        amount = Random.RandomRange(10.0f, 35.0f);
	}
	
	// Update is called once per frame
	void Update () {

        //Sway Rotation
        pos = Mathf.Sin(Time.time * speed) * amount;
        transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, pos));
	}
}
