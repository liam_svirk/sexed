﻿using UnityEngine;
using System.Collections;

public class playerScript : MonoBehaviour {

    Vector3 pos;
    float yMove;
    float amount= 0.025f;
    public bool move;
	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.transform.tag == "pointer")
        {
            move = false;
        }
    }
	
	// Update is called once per frame
	void Update () {

        //Sway/Walking Animation
        yMove = Mathf.Sin(Time.time * 15.0f) * amount;
        //
        pos.y += yMove;
        transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, yMove*200.0f));

        if(move)
        {
            //if player is moving, then LERP to pointer. Adjust walking animation 'amount'
            pos = Vector3.Lerp(pos, GameObject.Find("pointer").transform.position, Time.deltaTime * 2.0f);
            amount = Mathf.Lerp(amount, 0.035f, Time.deltaTime*5.0f);
        }
        else
        {
            //'amount' gets set to 0, to stop walking animation
            amount = Mathf.Lerp(amount, 0.0025f, Time.deltaTime*3.0f);
        }
        
        pos.z = 0.0f;
        transform.position = pos;


        
	}
}
