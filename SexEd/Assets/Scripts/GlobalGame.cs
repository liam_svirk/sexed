﻿using UnityEngine;
using System.Collections;

public class GlobalGame : MonoBehaviour {

    Vector3 pos;
    Vector3 scale;
    Vector3 defaultScale;
    float opacity;
    float yMove;
    float amount = 0.01f;
	// Use this for initialization
	void Start () {
        
        defaultScale = transform.localScale;
	}


    void OnTriggerStay2D(Collider2D col)
    {
        if (col.transform.tag == "Player")
        {
            opacity = Mathf.Lerp(opacity, 0.0f, Time.deltaTime * 35.0f);
        }
    }
	
	// Update is called once per frame
	void Update () {
        yMove = Mathf.Sin(Time.time * 8.0f) * amount;
        transform.position += new Vector3(0.0f, yMove, 0.0f);
        if (Input.GetMouseButtonDown(0))
        {
            GameObject.Find("player").GetComponent<playerScript>().move = true;
            pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0.0f;
            scale = defaultScale * 3.0f;

            opacity = 1.0f;
            transform.position = pos;
        }

        

       // 
        scale = Vector3.Lerp(scale, defaultScale, Time.deltaTime * 10.0f);
        transform.localScale = scale;
        GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, opacity);
        
	}
}
