﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueScript : MonoBehaviour {

    DialogueParser parser;

    string dialogue;
    string charName;
    //int pose
    //string side, left or right on screen var

    bool showConversation = true;

    public Text dialogueBox;
    public Text nameBox;
    public Image portrait;

    int lineNum;


    // Use this for initialization
    void Start () {
        dialogue = "";
        charName = "";

        lineNum = 0;

        dialogueBox = GameObject.Find("DialogueBox_Text").GetComponent<Text>();
        nameBox = GameObject.Find("CharacterName_Text").GetComponent<Text>();
        portrait = GameObject.Find("CharacterPortrait").GetComponent<Image>();

        parser = GetComponent<DialogueParser>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && showConversation == true)
        {
            if (ParseLine())
            {

                DisplayText();
                DisplayPortrait();

                lineNum++;
            }
        }

    }

    //return true if succesful
    bool ParseLine()
    {
        charName = parser.GetName(lineNum);
        dialogue = parser.GetContent(lineNum);
        return true;
    }

    //Dialogue and name
    void DisplayText()
    {
        dialogueBox.text = dialogue;
        nameBox.text = charName;
    }

    //Char portrait
    void DisplayPortrait()
    {

        if (charName != "")
        {
            portrait.sprite = Resources.Load<Sprite>(charName);
        }
    }



    //Make dialogue box appear on screen
    void startDialogue()
    {
        showConversation = true;
    }
}
